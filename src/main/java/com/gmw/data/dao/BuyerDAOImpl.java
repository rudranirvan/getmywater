package com.gmw.data.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gmw.biz.exceptions.DataLayerException;
import com.gmw.commons.pojo.BuyerDO;

@Repository
public class BuyerDAOImpl implements BuyerDAO {

	private static final Logger LOG = LoggerFactory.getLogger(BuyerDAOImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean addBuyer(BuyerDO buyerDO) throws DataLayerException {
		Session session = null;

		try {
			session = sessionFactory.openSession();

			session.beginTransaction();
			session.save(buyerDO);
			session.getTransaction().commit();

		} catch (Exception e) {
			throw new DataLayerException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}

		return true;

	}

	@Override
	public BuyerDO getBuyerByEmail(String email) throws DataLayerException {
		BuyerDO userDO = new BuyerDO();

		Session session = null;

		try {
			session = sessionFactory.openSession();

			Query query = session.getNamedQuery("getBuyerByEmail").setString("email", email);
			List result = query.list();

			userDO = (BuyerDO) result.get(0);

		} catch (Exception e) {
			throw new DataLayerException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}

		return userDO;
	}

	@Override
	public BuyerDO getBuyerByPhone(String phone) throws DataLayerException {
		BuyerDO userDO = new BuyerDO();

		Session session = null;

		try {
			session = sessionFactory.openSession();

			Query query = session.getNamedQuery("getBuyerByPhone").setString("phone", phone);
			List result = query.list();

			userDO = (BuyerDO) result.get(0);

		} catch (Exception e) {
			throw new DataLayerException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return userDO;
	}

	@Override
	public BuyerDO getBuyerByBuyerId(String buyerId) throws DataLayerException {
		BuyerDO userDO = new BuyerDO();

		Session session = null;

		try {
			session = sessionFactory.openSession();

			Query query = session.getNamedQuery("getBuyerByBuyerId").setString("userId", buyerId);
			List result = query.list();

			userDO = (BuyerDO) result.get(0);

		} catch (Exception e) {
			throw new DataLayerException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return userDO;
	}

}
