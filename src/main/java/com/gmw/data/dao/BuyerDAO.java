package com.gmw.data.dao;

import com.gmw.biz.exceptions.DataLayerException;
import com.gmw.commons.pojo.BuyerDO;

public interface BuyerDAO {

	public boolean addBuyer(BuyerDO buyer) throws DataLayerException;
	
	public BuyerDO getBuyerByEmail(String email) throws DataLayerException;
	
	public BuyerDO getBuyerByPhone(String phone) throws DataLayerException;
	
	public BuyerDO getBuyerByBuyerId(String buyerId) throws DataLayerException;
	
}
