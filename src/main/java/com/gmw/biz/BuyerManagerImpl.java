package com.gmw.biz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gmw.biz.exceptions.DataLayerException;
import com.gmw.commons.pojo.BuyerDO;
import com.gmw.data.dao.BuyerDAOImpl;

@Repository
public class BuyerManagerImpl implements BuyerManager {

	@Autowired
	private BuyerDAOImpl buyerDAO;

	@Override
	public boolean addBuyer(BuyerDO userDO) throws DataLayerException {
		return buyerDAO.addBuyer(userDO);
	}

	@Override
	public BuyerDO getBuyerByEmail(String emailId) throws DataLayerException {
		return buyerDAO.getBuyerByEmail(emailId);
	}

	@Override
	public BuyerDO getBuyerByPhone(String phone) throws DataLayerException {
		return buyerDAO.getBuyerByPhone(phone);
	}

	@Override
	public BuyerDO getBuyerByBuyerId(String buyerId) throws DataLayerException {
		return buyerDAO.getBuyerByBuyerId(buyerId);
	}

}
