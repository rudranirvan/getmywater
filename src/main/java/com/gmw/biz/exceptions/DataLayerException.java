package com.gmw.biz.exceptions;

public class DataLayerException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private String msg = null;
	
	public DataLayerException() {
		super();
	}
	
	public DataLayerException(Exception e) {
		super(e);
	}
	
	public DataLayerException(String msg, Exception e) {
		super(msg, e);
		this.msg = msg;
	}
	
	@Override
	public String toString() {
		return msg;
	}
	
	@Override
	public String getMessage() {
		return msg;
	}
	
}
