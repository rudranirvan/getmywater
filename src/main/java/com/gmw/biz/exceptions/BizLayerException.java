package com.gmw.biz.exceptions;

public class BizLayerException extends Exception {

	private static final long serialVersionUID = 1L;

	private String msg = null;

	public BizLayerException() {
		super();
	}

	public BizLayerException(Exception e) {
		super(e);
	}

	public BizLayerException(String msg, Exception e) {
		super(msg, e);
		this.msg = msg;
	}

	@Override
	public String toString() {
		return msg;
	}

	@Override
	public String getMessage() {
		return msg;
	}

}
