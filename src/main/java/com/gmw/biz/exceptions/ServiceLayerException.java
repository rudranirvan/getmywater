package com.gmw.biz.exceptions;

public class ServiceLayerException extends Exception {

	private static final long serialVersionUID = 1L;

	private String msg = null;

	public ServiceLayerException() {
		super();
	}

	public ServiceLayerException(Exception e) {
		super(e);
	}

	public ServiceLayerException(String msg, Exception e) {
		super(msg, e);
		this.msg = msg;
	}

	@Override
	public String toString() {
		return msg;
	}

	@Override
	public String getMessage() {
		return msg;
	}

}
