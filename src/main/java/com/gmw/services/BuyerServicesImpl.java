package com.gmw.services;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gmw.biz.BuyerManagerImpl;
import com.gmw.biz.exceptions.DataLayerException;
import com.gmw.commons.pojo.BuyerDO;
import com.gmw.services.responses.RowAddedResponse;

@Path("/buyers")
@Service
public class BuyerServicesImpl implements BuyerServices {

	Logger LOG = LoggerFactory.getLogger(BuyerServicesImpl.class);

	@Autowired
	private BuyerManagerImpl userManager;

	@Override
	public Response addBuyer(BuyerDO buyerDO) {
		RowAddedResponse response = new RowAddedResponse();

		try {
			userManager.addBuyer(buyerDO);
			response.setRowAdded(true);
		} catch (DataLayerException e) {
			response.setRowAdded(false);
			LOG.error("Failed to addBuyer due to: " + e, e);
		}

		return Response.ok().header("Access-Control-Allow-Origin", "*").entity(response).build();
	}

	@Override
	public Response getBuyerByEmail(String email) {
		BuyerDO buyerDO = null;

		try {
			buyerDO = userManager.getBuyerByEmail(email);
		} catch (DataLayerException e) {
			LOG.error("Failed to getBuyerByEmail due to: " + e, e);
		}

		return Response.ok().header("Access-Control-Allow-Origin", "*").entity(buyerDO).build();
	}

	@Override
	public Response getBuyerByPhone(String phone) {
		BuyerDO buyerDO = null;

		try {
			buyerDO = userManager.getBuyerByPhone(phone);
		} catch (DataLayerException e) {
			LOG.error("Failed to getBuyerByPhone due to : " + e, e);
		}

		return Response.ok().header("Access-Control-Allow-Origin", "*").entity(buyerDO).build();
	}

	@Override
	public Response getBuyerByBuyerId(String buyerId) {
		BuyerDO buyerDO = null;

		try {
			buyerDO = userManager.getBuyerByBuyerId(buyerId);
		} catch (DataLayerException e) {
			LOG.error("Failed to getBuyerByUserId due to: " + e, e);
		}

		return Response.ok().header("Access-Control-Allow-Origin", "*").entity(buyerDO).build();
	}

}
