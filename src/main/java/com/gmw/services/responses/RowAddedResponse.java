package com.gmw.services.responses;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RowAddedResponse {

	boolean rowAdded;

	public boolean isRowAdded() {
		return rowAdded;
	}

	public void setRowAdded(boolean rowAdded) {
		this.rowAdded = rowAdded;
	}

}
