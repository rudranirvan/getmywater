package com.gmw.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.gmw.commons.pojo.BuyerDO;

public interface BuyerServices {

	@POST
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addBuyer(BuyerDO buyer);

	@GET
	@Path("/email/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBuyerByEmail(@PathParam("email") String email);

	@GET
	@Path("/phone/{phone}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBuyerByPhone(@PathParam("phone") String phone);

	@GET
	@Path("/buyerId/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBuyerByBuyerId(@PathParam("buyerId") String buyerId);

}
