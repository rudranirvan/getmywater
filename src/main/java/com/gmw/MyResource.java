package com.gmw;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Service;

@Service
@Path("/hi")
public class MyResource {

	public MyResource() {
		System.out.println("hi");
	}
	
	@Path("/myresouce")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getResource() {
		return "API works!";
	}
}
